import { suite, test, only, retries } from "mocha-typescript";
import { IUserRepo } from "interfaces/repos";
import UserRepo from "../../components/repos/user_repo";
import UserManager from "../../components/managers/user_manager";
import { IUser } from "interfaces/descriptors";
import { deepStrictEqual, strictEqual } from "assert";
import { IUserManager } from "interfaces/managers";

@suite
class UserManagerTest {
    private userRepo: IUserRepo;
    private userManager: IUserManager;
    private user1: IUser = {
        username: "1",
        name: "1",
        email: "1@gmail.com",
        password: "11111"
    };

    private user2: IUser = {
        username: "2",
        name: "2",
        email: "2@gmail.com",
        password: "11111"
    };

    private user3: IUser = {
        username: "3",
        name: "3",
        email: "3@gmail.com",
        password: "11111"
    };

    private user4: IUser = {
        username: "4",
        name: "4",
        email: "4@gmail.com",
        password: "11111"
    };

    private user5: IUser = {
        username: "5",
        name: "5",
        email: "5@gmail.com",
        password: "11111"
    };

    constructor() {
        this.userRepo = new UserRepo();
        this.userManager = new UserManager(this.userRepo);
    }

    after() {
        this.userRepo.clear();
    }

    @test
    "should be able to register"() {
        const user = this.userManager.register(this.user1);
        deepStrictEqual(user, this.user1);
    }

    @test
    "should throw error when registering user without username, email, or password"() {
        try {
            this.userManager.register({
                email: "e@gmail.com",
                password: "eeeee"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }

        try {
            this.userManager.register({
                username: "e",
                password: "eeeee"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }

        try {
            this.userManager.register({
                username: "e",
                email: "e@gmail.com"
            } as any);
        } catch (error) {
            strictEqual(
                error.message,
                "Username, email, and password are required."
            );
        }
    }

    @test
    "should throw error when registering user if another user with the same username exists"() {
        this.userManager.register(this.user1);
        try {
            this.userManager.register(this.user1)
        } catch (error) {
            strictEqual(
                error.message,
                `User with username ${this.user1.username} already exists.`
            );
        }
    }

    @test
    "should be able to read user"() {
        const registerUser = this.userManager.register(this.user1);
        const read = this.userManager.readUser(this.user1.username);
        deepStrictEqual(read, registerUser);
    }

    @test
    "should throw error when reading user if user not found"() {
        this.userManager.register(this.user1);
        try {
            this.userManager.readUser(this.user1.username)
        } catch (error) {
            strictEqual(
                error.message,
                `Username ${this.user1.username} not found`
            );
        }
    }

    // @test
    // "should be able to delete user"() {}

    // @test
    // "should throw error when deleting user if user not found"() {}

    // @test
    // "should be able to update user"() {
    //     this.userManager.register(this.user1);
    //     const updatedUser = this.userManager.updateUser(this.user1.username, {
    //         name: "11"
    //     } as any);
    //     const user = { ...this.user1 };
    //     user.name = "11";
    //     deepStrictEqual(updatedUser, user);
    // }

    // @test
    // "should throw error when updating user if user not found"() {}

    // @test
    // "should throw error when updating user if user object contains password"() {}

    // @test
    // "should be able to update user's password"() {}

    // @test
    // "should throw error when updating user's password if user not found"() {}

    // @test
    // "should throw error when updating user's password if old password is not the same as new password"() {}

    // @test
    // "should be able to login"() {}

    // @test
    // "should throw error when login if password is wrong"() {}
    // @test
    // "should throw error when login if user does not exist"() {}

    @test
    "should be able to list user"() {
        this.userManager.register(this.user1);
        this.userManager.register(this.user2);
        this.userManager.register(this.user3);
        this.userManager.register(this.user4);
        this.userManager.register(this.user5);

        const users = this.userManager.listUser({});
        strictEqual(users.data.length, 5);
        deepStrictEqual(users, {
            page: 1,
            limit: 10,
            total: 5,
            data: [this.user1, this.user2, this.user3, this.user4, this.user5]
        });
    }

    // @test
    // "should be able to filter user when listing user"() {}

    // @test
    // "should be able to sort user when listing user"() {}

    // @test
    // "should be able to paginate user when listing user"() {}

    // @test
    // "should be able to list users in descending order"() {}
}
