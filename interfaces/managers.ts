import { Paginated } from "./repos";
import { IUser } from "./descriptors";

export interface IUserManager {
    listUser(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): Paginated<IUser>;
    // descendingUsername(): Paginated<IUser>;
    readUser(username: string): IUser;
    // deleteUser(username: string): boolean;
    register(object: IUser): IUser;
    // login(username: string, password: string): boolean;
    // updateUser(username: string, object: IUser): IUser;
    // updatePassword(
    //     username: string,
    //     oldPassword: string,
    //     newPassword: string
    // ): IUser;
}
