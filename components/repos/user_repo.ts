import { IUserRepo } from "interfaces/repos";
import { IUser } from "interfaces/descriptors";
import * as _ from "lodash";
import { Component } from "merapi";

export default class UserRepo extends Component implements IUserRepo {
    constructor() {
        super();
    }

    private users: { [username: string]: IUser } = {};

    public list(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): IUser[] {
        let results = _.flatMap(this.users);
        if(options){
            const { page, limit, sort } = options;
            if(options.page && options.limit){
                results =  _.slice(results, page, page + limit);
            }

            if(sort){
                const column = String(_.head(_.keys(sort)));
                const sortby = String(_.head(_.values(sort)));
                results = _.orderBy(results, column, sortby)
            }
        }
        if(!_.isEmpty(query)){
            const column = String(_.head(_.keys(query)));
            const sortby = String(_.head(_.values(query)));
            results = _.filter(results, [column , sortby]);
        }
        return results;
    }

    public count(query: Object): number {
        if(!_.isEmpty(query)){
            const value = String(_.head(_.values(query)));
            if (!this.users[value]){
                return 0;
            }
        }
        return _.flatMap(this.users).length;
    }

    public create(object: IUser): IUser {
        const { username } = object;
        if (this.users[username]) {
            return null;
        }
        this.users[username] = object;
        return object;
    }

    public read(username: string): IUser {
        if(_.isEmpty(this.users[username])){
            return null;
        }
        return this.users[username];
    }

    public update(username: string, object: Partial<IUser>): IUser {
        if(_.isEmpty(this.users[username])){
            return null
        }
        return _.merge(this.users[username], object);
    }

    public delete(username: string): boolean {
        if(this.users[username]){
            delete this.users[username];
        }
        return false;
    }

    public clear(): void {
        this.users = {};
    }
}
